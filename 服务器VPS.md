##服务器

我用的[DigitalOcean](https://www.digitalocean.com/)(非广告0.0)

* 机器配置我用的最低的
* 选节点(机房)前自己[测速](http://speedtest-nyc1.digitalocean.com/)
* 操作系统：Ubuntu FreeBSD Fedora Debian CoreOS CentOS
* 支付方式：信用卡/PayPal

1. 注册账号
2. 选产品 节点 加SSH 起主机名 交钱 应该会给$10
3. 到Networking中加Domain[^1] 加Record(A、CNAME)[^2]  [^3] ，没域名的话可暂时跳过
4. 登录主机[^4]

[^1]:见“域名注册流程.md”
[^2]:https://www.digitalocean.com/community/tutorials/how-to-set-up-a-host-name-with-digitalocean
[^3]:https://my.oschina.net/xoyo/blog/196962
[^4]:https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-centos-quickstart